# docker build -f builder.Dockerfile -t gitlab-registry.cern.ch/cms-cactus/path/to/my/repo/builder:$(date +%F) .

# this dockerfile creates an image that contains everything needed to build this codebase, and deploy to cactus-review

FROM cern/c8-base:20200401-1.x86_64

ENV GO_VERSION=1.14
ENV K8S_VERSION=1.18.1
ENV PATH="$PATH:/usr/local/go/bin"

# yum packages
RUN yum install -y make

# golang
RUN curl -o go.tar.gz https://dl.google.com/go/go${GO_VERSION}.linux-amd64.tar.gz && \
    tar -C /usr/local -xzf go.tar.gz && rm -f go.tar.gz

# kubectl
RUN curl -Lo /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v${K8S_VERSION}/bin/linux/amd64/kubectl && chmod +x /usr/local/bin/kubectl