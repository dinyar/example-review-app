SHELL:=/bin/bash
.SUFFIXES: # Clear built-in rules
.DEFAULT_GOAL := build

build/app: $(shell find . -type f -name '*.go')
	mkdir -p build
	GOOS=linux CGO_ENABLED=0 go build -ldflags '-w -s' -a -installsuffix cgo -o $@ app.go

.PHONY: build
build: build/app

.PHONY: clean
clean:
	rm -rf build